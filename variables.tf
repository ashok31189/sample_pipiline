variable "region" {
  default = "ap-south-1"

}

variable "ami" {
  default = "ami-0f5ee92e2d63afc18"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "pem_mumbai"
}

variable "private_key_path" {
  default = "C:/Users/skash/Downloads/pem_mumbai.pem"
}
